# Praxisaufgabe | Mitgliederverwaltung

Loginbereich für registrierte Benutzer:innen. Registrierte Benutzer:innen haben eine Übersicht aller User-Einträge der Datenbank auf Ihrem Dashboard und können in einem gesonderten View weitere Benutzer:innen per Eingabe über Formulare hinzufügen. Das Abmelden der Benutzer:innen führt auf den Loginbereich.

Basiert auf Python/Django und nutzt insbesondere die Funktionalität von django.contrib.auth.
Layout basiert auf Templates mit HTML und CSS von Bootstrap.

### 1. Repository klonen

```python
git clone https://gitlab.com/akkakaakk/landeshauptarchiv
cd landeshauptarchiv
```

### 2. Virtual Environment erstellen

```python
python3 -m venv venv
```

### 3. Virtual Environment aktivieren

```python
Windows: venv\Scripts\activate
Unix or macOS: source venv/bin/activate
```

### 4. Dependencies installieren

```python
pip install -r requirements.txt
```
### 5. SECRET_KEY generieren 

```python
python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'
```

### 6. .env erstellen und SECRET_KEY hinzufügen 

```python
Windows: echo "SECRET_KEY=replace_with_secret_key" | Out-File -FilePath .env -Encoding utf8
Unix or macOS: echo "SECRET_KEY=replace_with_secret_key" > .env
```

### 7. Datenbank migrieren

```python
python manage.py migrate
```

### 8. Testnutzer:innen in Datenbank einspielen

```python
python manage.py inject_users
```

### 9. Development Server starten

```python
python manage.py runserver
```

### 10. Testnutzer:innen
| user | pwd |
| ------ | ------ |
|  eschneider      |  schneider123      |
|  mahmad      | ahmad123       |
|  aschulz      |   schulz123     |

### Hinweis

Backend und Frontend sind nicht getrennt und werden beide aus Django generiert. Dies entspricht dem gängigen Trend und dem aktuellen Kenntnisstand des Entwicklers. Sie laufen nicht auf getrennten Ports. Das ist Teil der Anforderung und wäre unter anderem aus Sicherheitsaspekten und für eine bessere Skalierbarkeit noch einzurichten. Damit muss sich der Entwickler allerdings nochmal genauer befassen.
