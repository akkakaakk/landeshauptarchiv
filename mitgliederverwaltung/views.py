from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import logout
from .forms import UserForm

@login_required
def dashboard(request):
    all_users = User.objects.all()
    return render(request, 'registration/dashboard.html', {'users': all_users})

@login_required
def register_user(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('dashboard')
    else:
        form = UserForm()

    return render(request, 'registration/register.html', {'form': form})


def logout_user(request):
        logout(request)
        return redirect('dashboard')