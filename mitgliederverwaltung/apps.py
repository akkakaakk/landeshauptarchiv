from django.apps import AppConfig


class MitgliederverwaltungConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mitgliederverwaltung'
