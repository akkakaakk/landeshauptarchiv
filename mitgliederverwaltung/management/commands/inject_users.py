# myapp/management/commands/inject_users.py

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

class Command(BaseCommand):
    help = 'Injects three example users into the database'

    def handle(self, *args, **options):
        # Check if there are already users in the database
        if User.objects.exists():
            self.stdout.write(self.style.SUCCESS('Example users already exist. Aborting.'))
            return

        # Create three example users
        users_data = [
            {'username': 'eschneider', 'first_name': 'Emre', 'last_name': 'Schneider',
             'email': 'eschneider@praxisaufgabe.de', 'password': 'schneider123'},
            {'username': 'aschulz', 'first_name': 'Aylin', 'last_name': 'Schulz', 'email': 'aschulz@praxisaufgabe.de',
             'password': 'schulz123'},
            {'username': 'mahmad', 'first_name': 'Maya', 'last_name': 'Ahmad', 'email': 'mahmad@praxisaufgabe.de',
             'password': 'ahmad123'},
        ]

        for data in users_data:
            User.objects.create_user(
                username=data['username'],
                first_name=data['first_name'],
                last_name=data['last_name'],
                email=data['email'],
                password=data['password']
            )
            self.stdout.write(self.style.SUCCESS(f'Benutzer:in {data["username"]} erfolgreich eingespielt.'))
